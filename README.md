# Special Guacamole

> Telling by the repo name it does not have a specific topic

## Notive

`Blurry Fedora` and `Sharp Red Hat` are created using Gimp 2.10, project files are their folders.

## Wallpaper

|               | Preview                                                       |
| ------------- | ------------------------------------------------------------- |
| Blurry Fedora | ![Blurry Fedora PNG](blurry_fedora/blurry_fedora_preview.jpg) |
| PNG           | [Blurry Fedora PNG](blurry_fedora/blurry_fedora.png)          |
| JPG           | [Blurry Fedora JPG](blurry_fedora/blurry_fedora.jpg)          |
|               |                                                               |
| Sharp Red Hat | ![Sharp Red Hat PNG](sharp_redhat/sharp_redhat_preview.jpg)   |
| PNG           | [Sharp Red Hat PNG](sharp_redhat/sharp_redhat.png)            |
| JPG           | [Sharp Red Hat JPG](sharp_redhat/sharp_redhat.jpg)            |

## License

MIT

You can use everything in this repository as you wish.
If there is a  problem contact me via issue in this repository.

## Copyright

All rights belong to their respective owners.
